import { Component, OnInit } from '@angular/core';
import {LastGameServiceService} from '../last-game-service.service';
@Component({
  selector: 'app-last-game',
  templateUrl: './last-game.component.html',
  styleUrls: ['./last-game.component.css']
})
export class LastGameComponent implements OnInit {
title = "Last Game";
lastgame;
total;
games = [];

  constructor(private lastgameService: LastGameServiceService ) { }

  ngOnInit() {
    this.getTop();
  }

  getTop(){
    this.lastgameService.getLastGame().subscribe((data) => {
      console.log(data);
      this.total = data;
      var temp = this.total.matches;
      console.log(temp);
      var count = 0;
      var pos = temp.length - 1;

      // Set array length
      this.games.length = 1;
      // Generate array, limit to 1 for top 1 of the game
      while(pos > -1) {
        if(temp[pos].status == "FINISHED") {
          this.games[0] = temp[pos];
          pos = 0;
        }
        pos--;
      }
      console.log(this.games[0]);
  });
}

}
