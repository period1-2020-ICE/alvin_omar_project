import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllGameResComponent } from './all-game-res.component';

describe('AllGameResComponent', () => {
  let component: AllGameResComponent;
  let fixture: ComponentFixture<AllGameResComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllGameResComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllGameResComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
