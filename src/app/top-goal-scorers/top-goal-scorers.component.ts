import { Component, OnInit } from '@angular/core';
import { TopGoalScorersService } from "../top-goal-scorers.service";

@Component({
  selector: 'app-top-goal-scorers',
  templateUrl: './top-goal-scorers.component.html',
  styleUrls: ['./top-goal-scorers.component.css']
})
export class TopGoalScorersComponent implements OnInit {
  title = "Top 5 goal scorers";
  topGoal;
  goals = [];

  constructor(private topGoalService: TopGoalScorersService) { }

  ngOnInit() {
    this.getScorers();
  }

  getScorers() {
    this.topGoalService.getScorers().subscribe((data) => {
      console.log(data);

      // Can't bind the scorer data directly from json obj
      this.topGoal = data;
      var temp = this.topGoal.scorers;
      console.log(temp);
      var count = 0;
      var pos = 0;
      
      // Set array length
      this.goals.length = 5;
      
      // Populate array, limit to 5 for top 5 of our team
      var index = 0;
      while(pos < 5) {
        if(temp[index].team.name == "FC Barcelona") {
          this.goals[pos] = temp[index];
          pos++;
        }
        index++;
      }
    });
  }

}
