import { Component, OnInit } from '@angular/core';
import {LastGameResultService} from '../last-game-result.service';

@Component({
  selector: 'app-last-game-result',
  templateUrl: './last-game-result.component.html',
  styleUrls: ['./last-game-result.component.css']
})
export class LastGameResultComponent implements OnInit {
title = "Last Game Result";
total;
games = [];

  constructor(private lastgameresultService: LastGameResultService ) { }


  ngOnInit() {
    this.getTop();
  }

  getTop(){
    this.lastgameresultService.getLastGameResult().subscribe((data) => {
      console.log(data);
      this.total = data;
      var temp = this.total.matches;
      console.log(temp);
      var count = 0;
      var pos = 0;
      // Set array length
      this.games.length = 1;
      // Populate array, limit to 1 for top 1 of our team
      var index = 0;
      while(pos < 1) {
        this.games[pos] = temp[index];
        pos++;
        index++;
      }
  });
}

}
