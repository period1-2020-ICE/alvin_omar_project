import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastGameResultComponent } from './last-game-result.component';

describe('LastGameResultComponent', () => {
  let component: LastGameResultComponent;
  let fixture: ComponentFixture<LastGameResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastGameResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastGameResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
