import { Component, OnInit } from '@angular/core';
import {LastFiveGamesService} from '../last-five-games.service';

@Component({
  selector: 'app-last-five-game',
  templateUrl: './last-five-game.component.html',
  styleUrls: ['./last-five-game.component.css']
})
export class LastFiveGameComponent implements OnInit {
title = "Last Five Game";
total;
games = [];

  constructor(private lastfivegameService: LastFiveGamesService ) { }

  ngOnInit() {
    this.getTop();
  }

  getTop(){
    this.lastfivegameService.getLastFiveGame().subscribe((data) => {
      console.log(data);
      this.total = data;
      var temp = this.total.matches;
      console.log(temp);
      var count = 0;
      var pos = 0;

      // Set array length
      this.games.length = 5;
      
      // Genrate array, limit to 5 for top 5 of our team
      var index = 0;
      while(pos < 5) {
        this.games[pos] = temp[index];
        pos++;
        index++;
      }
  });
}

}
