import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { H2hMatchResRivalComponent } from './h2h-match-res-rival.component';

describe('H2hMatchResRivalComponent', () => {
  let component: H2hMatchResRivalComponent;
  let fixture: ComponentFixture<H2hMatchResRivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ H2hMatchResRivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(H2hMatchResRivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
